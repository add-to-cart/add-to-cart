import { ConfigService } from '../config/config.service';
import { environment } from './../../environments/environment';
import { CustomResponse } from '../model/custom-response';
import { catchError, Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private readonly apiUrl = environment.ORDER_SERVICE_URL + '/order';

  constructor(private http: HttpClient, private config: ConfigService) { }

  addOrder(order: any): Observable<CustomResponse> {
    return this.http.post<CustomResponse>(this.apiUrl, order, httpOptions).pipe(
      catchError(this.config.handleError)
    );
  }

  getOrders(customerEmail: string): Observable<CustomResponse> {

    return this.http.get<CustomResponse>(this.apiUrl + '/customer/' + customerEmail);
  }

  getAllOrders(): Observable<CustomResponse> {

    return this.http.get<CustomResponse>(this.apiUrl).pipe(
      catchError(this.config.handleError)
    );
  }

  confirmOrder(order: any): Observable<CustomResponse> {

    return this.http.put<CustomResponse>(this.apiUrl, order).pipe(
      catchError(this.config.handleError)
    );
  }

  getOrderStats(): Observable<CustomResponse> {

    return this.http.get<CustomResponse>(this.apiUrl + '/stats').pipe(
      catchError(this.config.handleError)
    );
  }
}

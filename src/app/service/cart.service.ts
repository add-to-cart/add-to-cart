import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public cartItemList: any = [];
  public products = new BehaviorSubject<any>([]);

  constructor() {

    // initialisze cart based on the content of localstorage (cart is store in localstorage)
    if (localStorage.getItem("cart") === null) {
      this.products = new BehaviorSubject<any>([]);
    } else {
      this.cartItemList = JSON.parse(localStorage.getItem("cart")!);
      this.products = new BehaviorSubject<any>(this.cartItemList);
    }
  }

  getProducts() {
    return this.products.asObservable();
  }

  setProducts(products: any) {
    this.cartItemList.push(...products);
    this.products.next(products);
  }

  // total price by item in cart
  itemTotalPrice(item: any) {
    item.total = item.price * item.quantity;
    item.total = item.total - (item.total * item.discountPercentage) / 100;
    return item.total;
  }

  addToCart(product: any) {

    // product item don't exist in cart
    let alreadyExist = false;

    // check if the item to add exist in the current cart
    for (const item of this.products.value) {
      if (item.id === product.id) {
        console.log(item);
        item.quantity++;
        item.total = this.itemTotalPrice(item);
        alreadyExist = true;
        break;
      }
    }

    // add the item if not found in the current cart
    if (!alreadyExist) {
      product.total = this.itemTotalPrice(product);
      this.cartItemList.push(product);
    }

    // update the observable for the cart
    this.products.next(this.cartItemList);

    // update the cart value in localstorage
    localStorage.setItem("cart", JSON.stringify(this.products.getValue()));
  }

  // update quantity of a product in cart
  updateCartItem(product: any, quantity: number) {
    for (const item of this.products.value) {
      if (item.id === product.id) {
        item.quantity = quantity;
        item.total = this.itemTotalPrice(item);
        break;
      }
    }
    this.products.next(this.cartItemList);

    localStorage.setItem("cart", JSON.stringify(this.products.getValue()));
  }

  // calculate total price of a cart
  getTotalPrice(): number {
    let grandTotal = 0;
    this.cartItemList.map((item: any) => {
      grandTotal += item.total;
    })
    return grandTotal;
  }

  // remove a product from a cart
  removeCartItem(product: any) {
    this.cartItemList.map((item: any, index: any) => {
      if (product.id === item.id) {
        this.cartItemList.splice(index, 1);
      }
    })
    this.products.next(this.cartItemList);

    localStorage.setItem("cart", JSON.stringify(this.products.getValue()));
  }

  removeAllCart() {
    this.cartItemList = [];
    this.products.next(this.cartItemList);

    localStorage.removeItem("cart");
  }
}

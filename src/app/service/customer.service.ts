import { ConfigService } from '../config/config.service';
import { CustomResponse } from './../model/custom-response';
import { environment } from './../../environments/environment.prod';
import { Observable, catchError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private readonly apiUrl = environment.ORDER_SERVICE_URL + '/customer';

  constructor(private http: HttpClient, private config: ConfigService) { }

  getAllCustomers(): Observable<CustomResponse> {

    return this.http.get<CustomResponse>(this.apiUrl).pipe(
      catchError(this.config.handleError)
    );
  }
}

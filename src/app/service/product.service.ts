import { ConfigService } from '../config/config.service';
import { Injectable } from '@angular/core';
import { Observable, catchError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  // private apiUrl = 'https://fakestoreapi.com/products';
  private apiUrl = 'https://dummyjson.com/products';

  constructor(private http: HttpClient, private config: ConfigService) { }

  getProducts(category: string): Observable<any> {

    // TODO: pagination
    // default pagination values
    let pageNumber: number = 1;
    let productQtyByPage: number = 9;

    if (category === undefined || category === "")
      // list of products with pagination
      return this.http.get<any>(`${this.apiUrl}?limit=${productQtyByPage}&skip=${productQtyByPage * (pageNumber - 1)}`).pipe(
        catchError(this.config.handleError)
      );
    else
      return this.http.get<any>(`${this.apiUrl}/category/${category}?limit=${productQtyByPage}&skip=${productQtyByPage * (pageNumber - 1)}`).pipe(
        catchError(this.config.handleError)
      );;
  }

  getCategories(): Observable<any> {

    return this.http.get<any>(this.apiUrl + "/categories").pipe(
      catchError(this.config.handleError)
    );;
  }
}
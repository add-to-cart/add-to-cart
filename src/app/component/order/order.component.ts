import { CustomerService } from './../../service/customer.service';
import { CookieService } from 'ngx-cookie-service';
import { OrderService } from './../../service/order.service';
import { Component, OnInit } from '@angular/core';
import { map, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orderStats: any = {};
  customers: any = [];
  orders = new BehaviorSubject<any>([]);

  constructor(private orderService: OrderService, private cookie: CookieService, private customerService: CustomerService) { }

  ngOnInit(): void {

    // retrieve orders as observer
    this.orderService.getAllOrders().subscribe(response => {

      this.orders = new BehaviorSubject<any>(response.data);
    })

    // retrieve orders statistics as observer
    this.updateStatsValue();

  }

  cancelOrder(order: any): void {
    order.status = "CANCELED"
    this.orderService.confirmOrder(order).subscribe(response => {
      this.orders.next(response.data);
      this.updateStatsValue();
    })
  }

  confirmOrder(order: any): void {
    order.status = "CONFIRMED"
    this.orderService.confirmOrder(order).subscribe(response => {
      this.orders.next(response.data);
      this.updateStatsValue();
    })
  }

  updateStatsValue(): void {
    this.orderService.getOrderStats().subscribe(response => {
      this.orderStats = response.data;
    })
  }

}

import { CartService } from './../../service/cart.service';
import { ProductService } from './../../service/product.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: any = [];
  categories: any = [];
  currentCategory: string = "";

  constructor(private productService: ProductService, private cartService: CartService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    // retrieve all products by category if param "category" added in the route
    this.route.params.subscribe(params => {

      // getting the param of the currentRoute
      this.currentCategory = params['category'];

      // retrieve all products
      this.productService.getProducts(this.currentCategory).subscribe((response) => {
        this.products = response.products;

        // add field "quantity" and "total" to productItem to use later in carts
        this.products.forEach((a: any) => {
          Object.assign(a, { quantity: 1, total: a.price });
        })
      });

    });

    // retrieve all categories
    this.productService.getCategories().subscribe((response) => {
      this.categories = response;
    })
  }

  // add product item to cart
  addToCart(item: any) {
    this.cartService.addToCart(item);
  }
}

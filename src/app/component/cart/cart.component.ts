import { CartService } from './../../service/cart.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public products: any = [];
  public grandTotal: number = 0;

  constructor(private cartService: CartService) { }

  ngOnInit(): void {

    // retrieve and subscribe to observable used for cart item values
    this.cartService.getProducts().subscribe(response => {
      this.products = response;

      // get total price of the cart
      this.grandTotal = this.cartService.getTotalPrice();
    });
  }

  // remove a product from a cart
  removeItemFromCart(product: any) {
    this.cartService.removeCartItem(product);
  }

  updateCart(product: any, event: any) {
    this.cartService.updateCartItem(product, event.target.value);
  }

  emptyCart() {
    this.cartService.removeAllCart();
  }
}

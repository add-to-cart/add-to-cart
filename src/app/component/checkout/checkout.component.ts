import { OrderService } from './../../service/order.service';
import { CookieService } from 'ngx-cookie-service';
import { CartService } from './../../service/cart.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  public products: any = [];
  public grandTotal: number = 0;

  public checkoutForm: FormGroup;

  // Form state
  loading = false;
  success = false;
  failed = false;
  message = "";

  constructor(private cartService: CartService, private formBuilder: FormBuilder,
    private cookie: CookieService,
    private orderService: OrderService, private router: Router) { }

  ngOnInit(): void {
    this.cartService.getProducts()
      .subscribe(response => {
        if (response === null || response.length === 0)
          this.router.navigate(['/products']);
        this.products = response;
        this.grandTotal = this.cartService.getTotalPrice();
        localStorage.setItem("cart", JSON.stringify(this.products));
      })

    this.checkoutForm = this.formBuilder.group({
      firstName: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ]],
      lastName: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ]],
      street1: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255),
      ]],
      street2: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255),
      ]],
      city: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
      ]],
      zip: ['', [
        Validators.required,
        Validators.pattern('^(([0-8][0-9])|(9[0-5]))[0-9]{3}$')
      ]],
      phone: ['', [
        Validators.required,
        //Validators.pattern('/^[+]?(?=(?:[^\dx]*\d){7})(?:\(\d+(?:\.\d+)?\)|\d+(?:\.\d+)?)(?:[ -]?(?:\(\d+(?:\.\d+)?\)|\d+(?:\.\d+)?))*(?:[ ]?(?:x|ext)\.?[ ]?\d{1,5})?$/')
      ]],
      email: ['', [
        Validators.required,
        Validators.email,
      ]],
      orderNote: ['', [
        Validators.required,
        Validators.maxLength(255),
      ]],
    })
  }

  async submitHandler() {
    this.loading = true;

    const formValue = this.checkoutForm.value;

    try {

      this.cookie.set('userData', JSON.stringify(formValue), { expires: 2, path: '/' })
      let order = '{ "userData": ' + JSON.stringify(formValue) + ', "carts": ' + JSON.stringify(this.products) + ', "grandTotal": ' + JSON.stringify(this.grandTotal) + '}';
      console.log(order);
      this.orderService.addOrder(order).subscribe(response => {
        console.log(response);
        if (response.statusCode === 201) {
          localStorage.removeItem("cart");

          setTimeout(() => {
            this.success = true;
            this.failed = false;
            this.message = "Order passed."
          }, 3000);

          setTimeout(() => {
            this.router.navigate(['/products']).then(() => {
              window.location.reload();
            });
          }, 4500);

        } else {

          setTimeout(() => {
            this.success = false;
            this.failed = true;
            this.message = "Failed to validate your data."
          }, 3000);

        }
      });

    } catch (error) {

      setTimeout(() => {
        this.success = false;
        this.failed = true;
        this.message = "Failed to validate your data."
      }, 3000);

      console.log(error)
    }

    setTimeout(() => {
      this.loading = false;
    }, 3000);
  }

  get firstName() {
    return this.checkoutForm.get('firstName');
  }

  get lastName() {
    return this.checkoutForm.get('lastName');
  }

  get street1() {
    return this.checkoutForm.get('street1');
  }

  get street2() {
    return this.checkoutForm.get('street2');
  }

  get city() {
    return this.checkoutForm.get('city');
  }

  get zip() {
    return this.checkoutForm.get('zip');
  }

  get phone() {
    return this.checkoutForm.get('phone');
  }

  get email() {
    return this.checkoutForm.get('email');
  }

  get orderNote() {
    return this.checkoutForm.get('orderNote');
  }

}

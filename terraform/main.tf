terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "add_to_cart" {
  name         = "wi11i4m/add-to-cart:latest"
  keep_locally = false
}

resource "docker_container" "add_to_cart" {
  image = docker_image.add_to_cart.latest
  name  = "add-to-cart"
  ports {
    internal = 80
    external = 4200
  }
}